import os
import json
import logging
import boto3
import botocore
import pymongo
import vcf_insert_db
import sys


class FailedOutputDownload(Exception):
    pass


def download_file(client, orderId, path, location):
    x = location.split("/", 4)
    bucket_name = x[3]
    obj_name = x[4]
    file_name = x[4].split("/")
    try:
        bucket = client.Bucket(bucket_name)
        obj = bucket.Object(obj_name)
        if obj is not None:
            suffix = ".vcf"
            x = file_name[-1].split(".", 2)
            if x[-1] == "gz" or x[-1] == "bgz":
                suffix += ".gz"
            path1 = file_name[-1]
            bucket.download_file(Filename=path1, Key=obj_name)
            return None, path1
    except botocore.exceptions.ClientError as err:
        return err, None
    return None, None


if __name__ == '__main__':

    netloc = os.getenv('S3_GATEWAY')
    outputs_bucket = "workflow-outputs"
    dir_path_outputs = "/app/storage/outputs/"

    # populate gnomad db based on the file in the following s3
    access_key = os.getenv('S3_ACCESS_KEY')
    secret_key = os.getenv('S3_SECRET_KEY')
    client = boto3.resource('s3',
                            endpoint_url="http://" + netloc,
                            aws_access_key_id=access_key,
                            aws_secret_access_key=secret_key)
    # qual_threshold = float(os.getenv('QUAL_THRESHOLD'))

    # populate_gnomad_common_db(client)

    with pymongo.MongoClient(os.getenv('MONGODB_ENDPOINT')+":"+os.getenv('MONGODB_PORT'),
                             username=os.getenv('MONGODB_USER'),
                             password=os.getenv('MONGODB_PASSWORD'),
                             authSource=os.getenv('MONGODB_AUTHENTICATION_SOURCE')) as mongoclient:
        mongodb = mongoclient[os.getenv('MONGODB_DATABASE')]
        metadata_documents = mongodb["metadata"]

        # loop through all groups in valid groups variable
        valid_groups = set(json.loads(os.environ.get("GROUPS")))
        for valid_group in valid_groups:
            group_cap = valid_group.upper().replace("-", "_")
            access_key = os.getenv('S3_' + group_cap + '_ACCESS_KEY')
            secret_key = os.getenv('S3_' + group_cap + '_SECRET_KEY')
            client = boto3.resource('s3',
                                    endpoint_url="http://" + netloc,
                                    aws_access_key_id=access_key,
                                    aws_secret_access_key=secret_key)
            mysql_host = os.getenv('MYSQL_HOST_' + group_cap)
            mysql_username = os.getenv('MYSQL_USERNAME_' + group_cap)
            mysql_password = os.getenv('MYSQL_PASSWORD_' + group_cap)
            mysql_database = os.getenv('MYSQL_DATABASE_' + group_cap)
            lovd_host = os.getenv('LOVD_HOST_' + group_cap)

            # loop through all metadata entries that are done by the loader, are succeeded and belong to this valid group
            success = True
            query = {"finished": 1, "group": valid_group,
                     "status": "Succeeded"}
            workflows = metadata_documents.find(query)
            for workflow in workflows:
                path = dir_path_outputs + workflow['orderId'] + " /"
                if not os.path.exists(path):
                    os.makedirs(path)
                outputs = workflow['outputs']
                # loop through all output files for this succeeded workflow
                failed = FailedOutputDownload()
                try:
                    for key in outputs.keys():
                        if '.vcf' in key or 'GATK-subworkflow-mutect2.cwl.coverage-reporter_out' in key:    
                            err, file = download_file(
                                client, workflow['orderId'], path, outputs[key]['location'])
                            if err is not None:
                                logging.error(err)
                                raise failed
                            elif file is None:
                                logging.error("Error while downloading vcf")
                                raise failed
                            # Store it in database
                
                            individual = workflow['orderId']
                            s3Location = outputs[key]['location']
                            vcf_insert_db.insert(
                                mysql_host, mysql_username, mysql_password, mysql_database, file, individual,s3Location)
                            # Cleaning
                            try:
                                os.remove(file)
                            except Exception as err:
                                logging.error(err)

                except FailedOutputDownload:
                    success = False
                    continue
                # update the state of this metadata entry so the vcf loader doesnt handle it again
                query = {"_id": workflow["_id"]}
                new_value = {"$set": {"finished": 2}}
                metadata_documents.update_one(query, new_value)
            if not success:
                raise Exception(
                    "There was an issue with finding or downloading outputs of a workflow")
